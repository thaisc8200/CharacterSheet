package com.itb.charactersheet.screens.details;


import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.itb.charactersheet.model.CharacterSheet;
import com.itb.charactersheet.repository.CharacterSheetRepository;

public class CharacterSheetDetailsViewModel extends AndroidViewModel {
    private CharacterSheetRepository repository;
    private CharacterSheet characterSheet;

    public CharacterSheetDetailsViewModel(@NonNull Application application) {
        super(application);
        this.repository = new CharacterSheetRepository(application);
    }

    public void loadCharacterSheet(int id) { characterSheet = repository.getCharacterSheetById(id);}

    public CharacterSheet getCharacterSheet(){ return characterSheet;}

    public void delete(int idCharacterSheet) { repository.delete(new CharacterSheet(idCharacterSheet));   }
}
