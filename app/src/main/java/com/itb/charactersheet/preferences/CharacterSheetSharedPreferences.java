package com.itb.charactersheet.preferences;

import android.content.Context;
import android.content.SharedPreferences;

public class CharacterSheetSharedPreferences {
    SharedPreferences sharedPreferences;
    public CharacterSheetSharedPreferences(Context context){
        sharedPreferences = context.getSharedPreferences("filename", Context.MODE_PRIVATE);
    }

    public void setNightModeState(Boolean state){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("NightMode", state);
        editor.commit();
    }

    public boolean loadNightModeState(){
        Boolean state = sharedPreferences.getBoolean("NightMode",false);
        return state;
    }
}
