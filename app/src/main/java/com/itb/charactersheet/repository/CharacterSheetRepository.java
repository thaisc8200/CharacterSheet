package com.itb.charactersheet.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.itb.charactersheet.dao.CharacterSheetDao;
import com.itb.charactersheet.db.CharacterSheetDatabase;
import com.itb.charactersheet.model.CharacterSheet;

import java.util.List;

public class CharacterSheetRepository {
    private CharacterSheetDao characterSheetDao;

    public CharacterSheetRepository(Application application) {
        CharacterSheetDatabase db = CharacterSheetDatabase.getDatabase(application);
        characterSheetDao = db.characterSheetDao();
    }

    public LiveData<List<CharacterSheet>> getAllCharacterSheets() {
        return characterSheetDao.getAllCharacterSheets();
    }

    public CharacterSheet getCharacterSheetById(int id){
        return characterSheetDao.getCharacterSheetById(id);
    }

    public LiveData<CharacterSheet> getCharacterSheetsById(int id){
        return characterSheetDao.getCharacterSheetsById(id);
    }


    public LiveData<List<CharacterSheet>> getCharacterSheetsByPlayer(String player){
        return characterSheetDao.getCharacterSheetsByPlayer(player);
    }


    public LiveData<List<CharacterSheet>> getCharacterSheetsByName(String name){
        return characterSheetDao.getCharacterSheetsByName(name);
    }


    public LiveData<List<CharacterSheet>> getCharacterSheetsByExactLevel(int level){
        return characterSheetDao.getCharacterSheetsByExactLevel(level);
    }


    public LiveData<List<CharacterSheet>> getCharacterSheetsByLowerLevel(int level){
        return characterSheetDao.getCharacterSheetsByLowerLevel(level);
    }


    public LiveData<List<CharacterSheet>> getCharacterSheetsByGreaterLevel(int level){
        return characterSheetDao.getCharacterSheetsByGreaterLevel(level);
    }

    public LiveData<List<CharacterSheet>> getCharacterSheetsOrderedByPlayer(){
        return characterSheetDao.getCharacterSheetsOrderedByPlayer();
    }


    public LiveData<List<CharacterSheet>> getCharacterSheetsOrderedByName(){
        return characterSheetDao.getCharacterSheetsOrderedByName();
    }


    public LiveData<List<CharacterSheet>> getCharacterSheetsOrderedByLevel(){
        return characterSheetDao.getCharacterSheetsOrderedByLevel();
    }

    public void delete(CharacterSheet characterSheet){
        new DeleteAsyncTask(characterSheetDao).execute(characterSheet);
    }

    private static class DeleteAsyncTask extends AsyncTask<CharacterSheet, Void, Void> {
        private CharacterSheetDao dao;
        public DeleteAsyncTask(CharacterSheetDao characterSheetDao) {
            dao = characterSheetDao;
        }

        @Override
        protected Void doInBackground(CharacterSheet... characterSheets) {
            dao.delete(characterSheets[0]);
            return null;
        }
    }

    public void insert(CharacterSheet characterSheet){
        new InsertAsyncTask(characterSheetDao).execute(characterSheet);
    }

    private static class InsertAsyncTask extends AsyncTask<CharacterSheet, Void, Void> {
        private CharacterSheetDao dao;
        public InsertAsyncTask(CharacterSheetDao characterSheetDao) {
            dao = characterSheetDao;
        }

        @Override
        protected Void doInBackground(CharacterSheet... characterSheets) {
            dao.insert(characterSheets[0]);
            return null;
        }
    }

    public void update(CharacterSheet characterSheet){
        new UpdateAsyncTask(characterSheetDao).execute(characterSheet);
    }

    private static class UpdateAsyncTask extends AsyncTask<CharacterSheet, Void, Void> {
        private CharacterSheetDao dao;
        public UpdateAsyncTask(CharacterSheetDao characterSheetDao) {
            dao = characterSheetDao;
        }

        @Override
        protected Void doInBackground(CharacterSheet... characterSheets) {
            dao.update(characterSheets[0]);
            return null;
        }
    }

}
